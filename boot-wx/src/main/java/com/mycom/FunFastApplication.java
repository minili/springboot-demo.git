package com.mycom;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * Spring Boot 应用启动类
 * @author xp
 */
@ComponentScan("com.mycom")
@SpringBootApplication
public class FunFastApplication {

	public static void main(String[] args) {
		SpringApplication.run(FunFastApplication.class, args);
	}
}
