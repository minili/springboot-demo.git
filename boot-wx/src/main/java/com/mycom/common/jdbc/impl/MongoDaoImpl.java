package com.mycom.common.jdbc.impl;

import com.mongodb.client.result.UpdateResult;
import com.mycom.common.jdbc.MongoDao;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by songcz on 2017/4/11.
 */
@Repository
public class MongoDaoImpl implements MongoDao {

    private MongoTemplate mongoTemplate;

    public MongoTemplate getMongoTemplate() {
        return mongoTemplate;
    }

    public void setMongoTemplate(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }


    @Override
    public void insert(Object o, String collectionName) {
        mongoTemplate.insert(o, collectionName);
    }

    @Override
    public <T> T findOne(Query query, String collectionName, Class<T> clazz) {
        return mongoTemplate.findOne(query, clazz, collectionName);
    }

    @Override
    public <T> List<T> findAll(Query query, String collectionName, Class<T> clazz) {
        return mongoTemplate.find(query, clazz, collectionName);
    }

    @Override
    public int update(Query query, Update update, String collectionName) {
        // todo 待验证
        UpdateResult updateResult = mongoTemplate.upsert(query, update, collectionName);
        return (int) updateResult.getModifiedCount();
    }

    @Override
    public void createCollection(String collectionName) {
        mongoTemplate.createCollection(collectionName);
    }

    @Override
    public <T> void remove(Query query, String collectionName) {
        mongoTemplate.remove(query, collectionName);
    }

    @Override
    public long count(Query query, String collectionName) {
        return mongoTemplate.count(query, collectionName);
    }

    @Override
    public <T> T findAndModify(Query query, Update update, String collectionName, Class<T> clazz) {
        return mongoTemplate.findAndModify(query, update, clazz, collectionName);
    }

    @Override
    public <T> List<T> findAggregation(Aggregation agg, String collectionName, Class<T> clazz) {
        return mongoTemplate.aggregate(agg, collectionName, clazz).getMappedResults();
    }


}
