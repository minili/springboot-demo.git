package com.mycom.common.aoplog;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

/**
 * 实现Web层的日志切面
 */
@Component
@Aspect
@Order(1)
public class WebLogAspect {
    private Logger log = LoggerFactory.getLogger(getClass());
    private ThreadLocal<Long> startTime = new ThreadLocal<>();

    /**
     * 定义一个切入点.
     * 解释下：
     * <p>
     * ~ 第一个 * 代表任意修饰符及任意返回值.
     * ~ 第二个 * 任意包名
     * ~ 第三个 * 定义在web包或者子包
     * ~ 第四个 * 任意方法
     * ~ .. 匹配任意数量的参数.
     */
    @Pointcut("(execution(public * com.mycom..*Controller.*(..)))")
    public void webLog() {
    }

    @Before("webLog()")
    public void doBefore(JoinPoint joinPoint) throws UnsupportedEncodingException {
        startTime.set(System.currentTimeMillis());

        // 接收到请求，记录请求内容
        log.info("========================= before start =======================");
        Signature signature = joinPoint.getSignature();
        MethodSignature methodSignature = (MethodSignature) signature;
        log.info("CLASS_METHOD : " + methodSignature.getDeclaringTypeName() + "." + methodSignature.getName());

        // 记录下请求内容
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if(attributes != null){
            HttpServletRequest request = attributes.getRequest();
            log.info("IP : " + request.getRemoteAddr());
        }

        log.info("请求参数:    名称     值");
        String[] argsNameArray  = methodSignature.getParameterNames();
        Object[] argsValueArray = joinPoint.getArgs();
        for (int i = 0; i < argsNameArray.length; i++) {
            log.info("args_name: " + argsNameArray[i]);

            String argValue = argsValueArray[i] != null ? argsValueArray[i].toString() : "";
            if (argsNameArray[i].contains("encode")) {
                String str = URLDecoder.decode(argValue, "utf-8");
                log.info("args_value: " + (str.length() > 200 ? str.substring(0, 200) + "..." : str));
            } else {
                log.info("args_value: " + (argValue.length() > 200 ? argValue.substring(0, 200) + "..." : argValue));
            }
        }

        log.info("========================= before end =========================");
    }

    @AfterReturning(returning="rvt", pointcut="webLog()")
    public void doAfterReturning(JoinPoint joinPoint, Object rvt) {
        // 处理完请求, 返回内容
        log.info("========================= after returning start ==============");
        Signature signature = joinPoint.getSignature();
        log.info("CLASS_METHOD : " + signature.getDeclaringTypeName() + "." + signature.getName());

        if(rvt != null){
            String str = rvt.toString();
            if (str.length() > 200) {
                str = str.substring(0, 200) + "...";
            }

            log.info("return 返回值:");
            log.info(str);
        }

        log.info("耗时（毫秒） : " + (System.currentTimeMillis() - startTime.get()));
        log.info("========================= after returning end ================");
    }
}
