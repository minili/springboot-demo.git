package com.mycom.common.wxpay.util;

/**
 * Created by Administrator on 2017/4/3.
 */
public class PayParameters {
    String appid;               // 公众号名称，由商户传入
    String attach;              // 附加数据，在查询API和支付通知中原样返回，可作为自定义参数使用。
    String mch_id;              // 商户号: String(32), 微信支付分配的商户号
    String nonce_str;           // 随机串 String(32)
    String sign;                // paySign, String(32)
    String body;                // 好运棋牌-金币充值",
    String out_trade_no;        // 商户订单号: String(32) 商户系统内部订单号，要求32个字符内、且在同一个商户号下唯一
    String total_fee;           // 交易金额: Int, 交易金额默认为人民币交易，接口中参数支付金额单位为【分】，参数值不能带小数。对账单中的交易金额单位为【元】。
    String spbill_create_ip;    // ip,             // 终端IP: String(16)	123.12.12.123, APP和网页支付提交用户端ip
    String notify_url;          // notify_url,           // 通知地址, 异步接收微信支付结果通知的回调地址，通知url必须为外网可访问的url，不能携带参数。
    String trade_type;          // JSAPI',              // 交易类型: JSAPI--公众号支付
    String openid;

    public String getAttach() {
        return attach;
    }

    public void setAttach(String attach) {
        this.attach = attach;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getMch_id() {
        return mch_id;
    }

    public void setMch_id(String mch_id) {
        this.mch_id = mch_id;
    }

    public String getNonce_str() {
        return nonce_str;
    }

    public void setNonce_str(String nonce_str) {
        this.nonce_str = nonce_str;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getOut_trade_no() {
        return out_trade_no;
    }

    public void setOut_trade_no(String out_trade_no) {
        this.out_trade_no = out_trade_no;
    }

    public String getTotal_fee() {
        return total_fee;
    }

    public void setTotal_fee(String total_fee) {
        this.total_fee = total_fee;
    }

    public String getSpbill_create_ip() {
        return spbill_create_ip;
    }

    public void setSpbill_create_ip(String spbill_create_ip) {
        this.spbill_create_ip = spbill_create_ip;
    }

    public String getNotify_url() {
        return notify_url;
    }

    public void setNotify_url(String notify_url) {
        this.notify_url = notify_url;
    }

    public String getTrade_type() {
        return trade_type;
    }

    public void setTrade_type(String trade_type) {
        this.trade_type = trade_type;
    }
}
