package com.mycom.common.wxpay;

/**
 * 常量 pay constants
 */
public class PayCnst {
    public static final String FAIL       = "FAIL";
    public static final String SUCCESS    = "SUCCESS";
    public static final String HMACSHA256 = "HMAC-SHA256";
    public static final String MD5        = "MD5";

    public static final String SIGN_ALGORITHMS = "SHA1WithRSA";
    public static final String FIELD_SIGN      = "sign";
    public static final String FIELD_SIGN_TYPE = "sign_type";

    public static final String SEND_RED_PAG_KEY = "NADCBiQKBgQDuJ*";
    public static final String LOTTERY_KEY = "NA*CBiQKB123gQDuJ*";
    /** 获取游戏信息_key */
    public static String GAME_INFO_KEY = "haoyungame*123";

    public static final String wxPublicName_GuanDan = "GuanDan";
    public static final String wxPublicName_Haoyun  = "Haoyun";
    public static final String wxPublicName_BiJi    = "BiJi";

    public static final int transState_fail    = 0;     // 转账失败
    public static final int transState_success = 1;     // 转账成功
    public static final int transState_ongoing = 2;     // 转账中
    public static final int transState_no      = 3;     // 未转账


    public static final int no_get_award   = 0;     // 奖励未领取
    public static final int hav_get_award  = 1;     // 奖励已领取
    public static final int in_hadle_award = 2;     // 奖励领取中
    public static final int fail_get_award = 3;     // 奖励领取失败

    public static final int openid_len = 28;        // openid length

    public static String GAME_INFO = "https://open.3399hy.com/serverpay/clubMgt/getSrvUrlAndSignType";

    public static final String rsa_public_key="MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDuJv+yENlf931PunexedsUgVs9XdbO/vIPyrD/ML6Y4vJVonA2Esac8pql6M0Se+PpDKidpYdO+Y/27OkGlJEoR7wdACkY9hw+KJIp6a1NMc/c0hJXb3UEm/UmaakK35n7bGS1N8gvSZQlHr4Eh1eCIQ1g7VPRrVyCzbffp1EOMwIDAQAB";
    public static final String rsa_private_key = "MIICeAIBADANBgkqhkiG9w0BAQEFAASCAmIwggJeAgEAAoGBAO4m/7IQ2V/3fU+6d7F52xSBWz1d1s7+8g/KsP8wvpji8lWicDYSxpzymqXozRJ74+kMqJ2lh075j/bs6QaUkShHvB0AKRj2HD4okinprU0xz9zSEldvdQSb9SZpqQrfmftsZLU3yC9JlCUevgSHV4IhDWDtU9GtXILNt9+nUQ4zAgMBAAECgYEAsjvNsd6aURBQYJMVcWXZaIdHWa4ZTeHQF7GCtfotKw7uftiLUmzK6DtJMlIA6IyADbLDnvh7Q8fSzuKPo7UczK7/hiJiJ1DszaCA7d7fRsGR3WGc5Jj+h+odjCwl32BtCUfi+zbxocGcl+i3zEYMUvOLpOQTxsb/uGd5tAC7n8ECQQD7Cxz7ajFlp4FHvmapXYAMZ0vuxW+VgdsSsdhZr+gMhx7Sp/K6lI8SxZceTLkoTAFm28lZyhxPTGigiT2e8aQtAkEA8tq6cdmeb2gClNcx8d5y6Lw07Nm2ggoKDK3fZxrwb2Gk/7j8T0mgh/y6UV2Eq57Z3/1FUgVUheevv0WfLj8X3wJBANvnoDubiinB0T1HTCaQANrcoWdHC7erSHvXzezCakKPGjDtbXKDL9EBxJ6GFqt16MRxQq+km6fbBsuZbKEOiwECQQCrNNwgCyUQsSiwuYzyULp9rME7ALkTr/QpwCIw0+MRAquJxGXLl8JeXCMV3XH8c35x1GGcUzmlNRLHK1PvDHfNAkBwe74cAv/0uu91KtpF1Vsju6Q4xxnjoxawuL0lRn7U9fEwb9B1GBHVtpJ7/ycLsLDInzdVrYyBUylNgypzK5ip";


}

