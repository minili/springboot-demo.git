package com.mycom.common.wxpay;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.mycom.common.utils.HttpClientUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 微信支付, 获取支付必要参数信息
 * todo: pay 切换微信支付测试账号 WdPayBaseConfig WdPayBaseConfig_Test
 * @author Administrator
 */
public class WdPayBase extends WdPayBaseConfig_Test {
    private static Logger log = LoggerFactory.getLogger(WdPayBase.class);

    /**
     * @return String[]{openid, unionid}
     */
    public static String[] getOpenidAndUnionid(String code, String gameId) {
        JSONObject json = get_access_token_json(code, gameId);

        String access_token = json.getString("access_token");
        String openid       = json.getString("openid");

        if ((openid == null) || (access_token == null) || "".equals(openid) || "".equals(access_token)) {
            return null;
        }

        StringBuilder urlSb2 = new StringBuilder();
        urlSb2.append("https://api.weixin.qq.com/sns/userinfo?");
        urlSb2.append("access_token=");
        urlSb2.append(access_token);
        urlSb2.append("&openid=");
        urlSb2.append(openid);
        urlSb2.append("&lang=zh_CN");

        String responseStr2 = HttpClientUtil.takeGet(urlSb2.toString());
        JSONObject responseJo2 = JSON.parseObject(responseStr2);
        log.info("===" + responseStr2);
        if (responseJo2.getString("errcode") != null) {
            return null;
        }

        String unionId = responseJo2.getString("unionid");
        if (unionId == null) {
            return null;
        }

        return new String[]{openid, unionId};
    }

    /**
     * 获取网页授权凭证
     *
     * @param code
     * @return
     */
    private static JSONObject get_access_token_json(String code, String gameId) {
        StringBuffer sb = new StringBuffer();
        // example_url: "https://api.weixin.qq.com/sns/oauth2/access_token?appid=xxx&secret=xxx&code=xxx&grant_type=authorization_code";
        String base_url  = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=";
        String after_url = "&grant_type=authorization_code";

        String wxPublicName = WdPayBase.gameWxPublic.get(gameId);
        String appid;
        String secret;
        if (PayCnst.wxPublicName_Haoyun.equals(wxPublicName)) {
            appid = WdPayBase.Haoyun.appid;
            secret = WdPayBase.Haoyun.secret;
        } else if (PayCnst.wxPublicName_GuanDan.equals(wxPublicName)) {
            appid = WdPayBase.GuanDan.appid;
            secret = WdPayBase.GuanDan.secret;
        } else {
            appid = WdPayBase.BiJi.appid;
            secret = WdPayBase.BiJi.secret;
        }

        sb.append(base_url).append(appid).append("&secret=").append(secret).append("&code=").append(code)
                .append(after_url);
        log.info("get_access_token_json 参数: " + sb.toString());
        /** access_token_json_str示例:
         *    {
         * 		"access_token":"ACCESS_TOKEN",
         * 		"expires_in":7200,
         * 		"refresh_token":"REFRESH_TOKEN",
         * 		"openid":"OPENID",
         * 		"scope":"SCOPE"
         *    }
         *
         */
        String access_token_json_str = HttpClientUtil.takeGet(sb.toString());

        log.info("access_token_json_str: ----------" + access_token_json_str);

        JSONObject json = JSONObject.parseObject(access_token_json_str);

        return json;
    }

}
