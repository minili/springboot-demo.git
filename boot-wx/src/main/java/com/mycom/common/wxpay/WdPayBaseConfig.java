package com.mycom.common.wxpay;

import java.util.HashMap;
import java.util.Map;

/**
 * 微信支付, 获取支付必要参数信息
 */
public class WdPayBaseConfig {
    public static final Integer CENT_RATIO = 100; // 金额--元转换分
    public static final boolean is_test    = false;
    // 这里是公众号网页支付的key, 不是公众号的key

    /* ========================== 微信提现 ========================== */
    public static final String check_name = "NO_CHECK";          // NO_CHECK：不校验真实姓名  FORCE_CHECK：强校验真实姓名

    public static Map<String, String> gameWxPublic = new HashMap<>();
    static {
        gameWxPublic.put("HY_NJ_GD", "GuanDan");
        gameWxPublic.put("1", "Haoyun");    // 南京麻将
        gameWxPublic.put("2", "Haoyun");    // 启东麻将
        gameWxPublic.put("3", "Haoyun");    // 徐州麻将
        gameWxPublic.put("4", "Haoyun");    // 晃晃
        gameWxPublic.put("9", "BiJi");      // 比鸡
        gameWxPublic.put("10", "GuanDan");  // 掼蛋麻将
        gameWxPublic.put("13", "GuanDan");  // 上海麻将
    }

    public class GuanDan {
        public static final String appid   = "";
        public static final String mch_id  = "";
        public static final String desc    = "";          // 企业付款描述信息
        public static final String secret  = "";
        public static final String web_key = ""; // 支付key
        public static final String cert_file_path  = "/opt/certificate/*.p12";
    }

    public class Haoyun {
        public static final String appid   = "";
        public static final String mch_id  = "";
        public static final String desc    = "";          // 企业付款描述信息
        public static final String secret  = "";
        public static final String web_key = ""; // 支付key
        public static final String cert_file_path  = "/opt/certificate/*.p12";
    }

    public class BiJi {
        public static final String appid   = "";
        public static final String mch_id  = "";
        public static final String desc    = "";          // 企业付款描述信息
        public static final String secret  = "";
        public static final String web_key = ""; // 支付key
        public static final String cert_file_path  = "/opt/certificate/*.p12";
    }
}
