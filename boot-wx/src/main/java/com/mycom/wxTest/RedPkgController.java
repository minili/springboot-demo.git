package com.mycom.wxTest;

import com.alibaba.fastjson.JSONObject;
import com.mycom.common.utils.MD5Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 不需输入验证码场景下: 玩家绑定, 发送红包
 * @author xp
 * Created on 2018/3/17
 */
@Controller
@RequestMapping("/redPkgSrv")
public class RedPkgController {
    private static Logger log = LoggerFactory.getLogger(RedPkgController.class);

    @Autowired
    private RedPkgService service;

    /**
     * mongoTaskDao.insert
     * 绑定 unionId
     */
    @RequestMapping("/bindUnionid")
    @ResponseBody
    public JSONObject bindUnionid(String code, String gameId) {
        return service.bindUnionid(code, gameId);
    }

    /**
     * 验证玩家 unionId 是否已绑定 openid
     */
    @RequestMapping("/vdUserBind")
    @ResponseBody
    public JSONObject vdUserBind(String unionId, String gameId) {
        return service.vdUserBind(unionId, gameId);
    }

    /**
     * 兑换==发红包接口
     */
    @RequestMapping("/sendRedPacket")
    @ResponseBody
    public JSONObject sendRedPacket(String f_record_id, String unionId, String num, String ip, String sign, String gameId) {
        JSONObject json = new JSONObject();
        json.put("result", false);
        json.put("msg", "发送失败");
        json.put("error", 0);

        /*================ 业务代码 start ===============*/
        json = service.sendRedPacket(f_record_id, unionId, num, ip, sign, gameId);
        /*================ 业务代码 end =================*/

        return json;
    }

    /**
     * 兑换==发红包接口
     */
    @RequestMapping("/sendRedPacketTest")
    @ResponseBody
    public JSONObject sendRedPacketTest(String f_record_id, String unionId, String num, String ip, String sign, String gameId) {
        String str = f_record_id + gameId + ip + num + unionId + "NADCBiQKBgQDuJ*";
        // String str = "44441116.226.158.1461obOPlvgOQZYoH-g5TXsJ4C3SEIngNADCBiQKBgDuJ*";
        String md5 = MD5Util.md5(str);
        System.out.println("md5: " + md5);
        return null;
    }

}
