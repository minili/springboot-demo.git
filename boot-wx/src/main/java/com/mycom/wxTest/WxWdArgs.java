package com.mycom.wxTest;

/**
 * Created by Administrator on 2017/4/3.
 */
public class WxWdArgs {
    public String mch_appid;
    public String mchid;
    public String nonce_str;
    public String sign;
    public String partner_trade_no;
    public String openid;
    public String check_name;
    public String amount;
    public String desc;
    public String spbill_create_ip;
    public String web_key;
    public String cert_file_path;

    public String getWeb_key() {
        return web_key;
    }

    public void setWeb_key(String web_key) {
        this.web_key = web_key;
    }

    public String getCert_file_path() {
        return cert_file_path;
    }

    public void setCert_file_path(String cert_file_path) {
        this.cert_file_path = cert_file_path;
    }

    public String getMch_appid() {
        return mch_appid;
    }

    public void setMch_appid(String mch_appid) {
        this.mch_appid = mch_appid;
    }

    public String getMchid() {
        return mchid;
    }

    public void setMchid(String mchid) {
        this.mchid = mchid;
    }

    public String getNonce_str() {
        return nonce_str;
    }

    public void setNonce_str(String nonce_str) {
        this.nonce_str = nonce_str;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getPartner_trade_no() {
        return partner_trade_no;
    }

    public void setPartner_trade_no(String partner_trade_no) {
        this.partner_trade_no = partner_trade_no;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public String getCheck_name() {
        return check_name;
    }

    public void setCheck_name(String check_name) {
        this.check_name = check_name;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getSpbill_create_ip() {
        return spbill_create_ip;
    }

    public void setSpbill_create_ip(String spbill_create_ip) {
        this.spbill_create_ip = spbill_create_ip;
    }
}
