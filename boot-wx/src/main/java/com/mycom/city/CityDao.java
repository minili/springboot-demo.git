package com.mycom.city;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface CityDao {

    List<City> getCityList();
}
