package com.mycom.city;

import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author xp
 *         Created on 2018/4/2
 */
@RestController
@RequestMapping("/city")
public class CityController {

    @Autowired
    private CityService cityService;

    @RequestMapping("getCityList")
    public JSONObject getCityList () {
        JSONObject resultJson = new JSONObject();
        List<City> cityList   = cityService.getCityList();
        resultJson.put("cityList", cityList);

        return resultJson;
    }

}
