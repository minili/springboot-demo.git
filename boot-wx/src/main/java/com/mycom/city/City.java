package com.mycom.city;

/**
 * @author xp
 *         Created on 2018/4/2
 */
public class City {
    private Integer id;

    private String name;

    private String dec;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDec() {
        return dec;
    }

    public void setDec(String dec) {
        this.dec = dec;
    }
}
