package com.mycom.city;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author xp
 *         Created on 2018/4/2
 */
@Service
public class CityService {
    private static Logger log = LoggerFactory.getLogger(CityService.class);

    @Autowired
    private CityDao cityDao;

    public List<City> getCityList() {
        List<City> cityList = cityDao.getCityList();
        log.debug(" cityList size: " + cityList.size());

        return cityList;
    }
}
