/*
Navicat MySQL Data Transfer

Source Server         : 本地
Source Server Version : 50722
Source Host           : localhost:3306
Source Database       : fun-fast

Target Server Type    : MYSQL
Target Server Version : 50722
File Encoding         : 65001

Date: 2018-11-15 11:20:30
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for manager
-- ----------------------------
DROP TABLE IF EXISTS `manager`;
CREATE TABLE `manager` (
  `managerId` int(5) unsigned NOT NULL AUTO_INCREMENT COMMENT '管理员id',
  `managerName` varchar(50) NOT NULL,
  `nickName` varchar(50) DEFAULT NULL,
  `password` varchar(50) NOT NULL,
  `managerLevelId` int(2) NOT NULL,
  PRIMARY KEY (`managerId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COMMENT='管理员表';

-- ----------------------------
-- Records of manager
-- ----------------------------
INSERT INTO `manager` VALUES ('1', 'admin', 'admin', '4297f44b13955235245b2497399d7a93', '1');
INSERT INTO `manager` VALUES ('2', 'cscscs', 'cscscs', '4297f44b13955235245b2497399d7a93', '1');

-- ----------------------------
-- Table structure for managertoken
-- ----------------------------
DROP TABLE IF EXISTS `managertoken`;
CREATE TABLE `managertoken` (
  `managerId` int(20) NOT NULL,
  `token` varchar(50) NOT NULL,
  `expireTime` varchar(15) DEFAULT NULL COMMENT '过期时间yyyyMMddHHmmss',
  `updateTime` varchar(15) DEFAULT NULL COMMENT '更新时间yyyyMMddHHmmss',
  PRIMARY KEY (`managerId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of managertoken
-- ----------------------------

