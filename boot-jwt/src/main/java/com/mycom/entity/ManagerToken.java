package com.mycom.entity;

/**
 * @author milicool
 * Created on 2018/11/12
 */
public class ManagerToken {
    //管理员ID
    public Integer managerId;
    //token
    public String token;
    //过期时间
    public String expireTime;
    //更新时间
    public String updateTime;

    public Integer getManagerId() {
        return managerId;
    }

    public void setManagerId(Integer managerId) {
        this.managerId = managerId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(String expireTime) {
        this.expireTime = expireTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }
}
