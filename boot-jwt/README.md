# SpringBoot集成JWT实现接口权限认证

## JWT

### 介绍

Json web token (JWT), 是为了在网络应用环境间传递声明而执行的一种基于JSON的开放标准（(RFC 7519).该token被设计为紧凑且安全的， 特别适用于分布式站点的单点登录（`SSO`）场景。JWT的声明一般被用来在身份提供者和服务提供者间传递被认证的用户身份信息， 以便于从资源服务器获取资源，也可以增加一些额外的其它业务逻辑所必须的声明信息，该token也可直接被用于认证，也可被加密。

### 优点：

体积小、传输快
支持跨域授权，因为跨域无法共享cookie
分布式系统中，很好地解决了单点登录问题

### 缺点：

因为JWT是无状态的，因此服务端无法控制已经生成的Token失效，是不可控的

### 使用场景

1. 认证，这是比较常见的使用场景，只要用户登录过一次系统，之后的请求都会包含签名出来的token，通过token也可以用来实现单点登录。
2. 交换信息，通过使用密钥对来安全的传送信息，可以知道发送者是谁、放置消息被篡改。

### 认证过程

一般是在请求头里加入Authorization，并加上Bearer标注

JWT官网有一张图描述了JWT的认证过程: 

![](C:\Users\Administrator\Desktop\jwt-diagram.png)



## Springboot集成JWT

### 项目克隆
项目名称 springboot-jwt
项目地址: https://gitee.com/minili/springboot-demo.git
如果觉得该项目对你有帮助或者有疑问的话, 欢迎加星, 评论

### 添加表
一个是管理员表, 一个是存放token表
在项目下的db文件夹

```
SET FOREIGN_KEY_CHECKS=0;

DROP TABLE IF EXISTS `manager`;
CREATE TABLE `manager` (
  `managerId` int(5) unsigned NOT NULL AUTO_INCREMENT COMMENT '管理员id',
  `managerName` varchar(50) NOT NULL,
  `nickName` varchar(50) DEFAULT NULL,
  `password` varchar(50) NOT NULL,
  `managerLevelId` int(2) NOT NULL,
  PRIMARY KEY (`managerId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COMMENT='管理员表';

INSERT INTO `manager` VALUES ('1', 'admin', 'admin', '4297f44b13955235245b2497399d7a93', '1');
INSERT INTO `manager` VALUES ('2', 'cscscs', 'cscscs', '4297f44b13955235245b2497399d7a93', '1');

DROP TABLE IF EXISTS `managertoken`;
CREATE TABLE `managertoken` (
  `managerId` int(20) NOT NULL,
  `token` varchar(50) NOT NULL,
  `expireTime` varchar(15) DEFAULT NULL COMMENT '过期时间yyyyMMddHHmmss',
  `updateTime` varchar(15) DEFAULT NULL COMMENT '更新时间yyyyMMddHHmmss',
  PRIMARY KEY (`managerId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

```

### POM.XML
主要是添加shiro, jwt, jdbc
```XML
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>com.mycom</groupId>
	<artifactId>funfast</artifactId>
	<version>0.0.1-SNAPSHOT</version>
	<packaging>jar</packaging>

	<name>funfast</name>
	<description>project for Spring Boot JWT</description>

	<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>2.0.1.RELEASE</version>
		<relativePath/>
	</parent>

	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
		<java.version>1.8</java.version>
		<mysql-connector>5.1.38</mysql-connector>
		<mybatis-plus-boot-starter.version>2.1.9</mybatis-plus-boot-starter.version>
		<druid.version>1.1.10</druid.version>
		<fastjson.version>1.2.39</fastjson.version>
		<jwt.version>0.7.0</jwt.version>
	</properties>

	<dependencies>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter</artifactId>
			<exclusions>
				<exclusion>
					<groupId>org.springframework.boot</groupId>
					<artifactId>spring-boot-starter-logging</artifactId>
				</exclusion>
			</exclusions>
		</dependency>

		<!-- Spring Boot web依赖 -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
		</dependency>
		<!-- log4j2 依赖 -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-log4j2</artifactId>
		</dependency>
		<!-- Spring Boot Test 依赖 -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-test</artifactId>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-configuration-processor</artifactId>
			<optional>true</optional>
		</dependency>

		<!-- Spring Boot JDBC 依赖 -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-jdbc</artifactId>
		</dependency>

		<!-- MySQL 连接驱动 依赖 -->
		<dependency>
			<groupId>mysql</groupId>
			<artifactId>mysql-connector-java</artifactId>
			<version>${mysql-connector}</version>
		</dependency>
		<!-- druid 连接池 依赖 -->
		<dependency>
			<groupId>com.alibaba</groupId>
			<artifactId>druid-spring-boot-starter</artifactId>
			<version>${druid.version}</version>
		</dependency>

		<!-- shiro 权限控制 -->
		<dependency>
			<groupId>org.apache.shiro</groupId>
			<artifactId>shiro-spring</artifactId>
			<version>1.4.0</version>
		</dependency>

		<!-- shiro ehcache (shiro缓存)-->
		<dependency>
			<groupId>org.apache.shiro</groupId>
			<artifactId>shiro-ehcache</artifactId>
			<version>1.4.0</version>
			<exclusions>
				<exclusion>
					<artifactId>slf4j-api</artifactId>
					<groupId>org.slf4j</groupId>
				</exclusion>
			</exclusions>
		</dependency>

		<!-- jwt -->
		<dependency>
			<groupId>io.jsonwebtoken</groupId>
			<artifactId>jjwt</artifactId>
			<version>${jwt.version}</version>
		</dependency>

		<!-- fastjson 依赖 -->
		<dependency>
			<groupId>com.alibaba</groupId>
			<artifactId>fastjson</artifactId>
			<version>${fastjson.version}</version>
		</dependency>

	</dependencies>

	<build>
		<plugins>
			<plugin>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-maven-plugin</artifactId>
			</plugin>
		</plugins>
	</build>

</project>

```

### 修改application.yml文件
```yml
# server
server:
   tomcat:
        uri-encoding: UTF-8
        max-threads: 1000
        min-spare-threads: 30
   port: 8087
   servlet:
        context-path: /

spring:
    # 环境 dev|prod
    profiles:
        active: dev

    servlet:
        multipart:
            max-file-size: 100MB
            max-request-size: 100MB
            enabled: true

    datasource:
      type: com.alibaba.druid.pool.DruidDataSource
      driver-class-name: com.mysql.jdbc.Driver
      druid:
        url: jdbc:mysql://127.0.0.1:3306/fun-fast?useUnicode=true&characterEncoding=UTF-8&zeroDateTimeBehavior=convertToNull
        username: root
        password: 123123

        initial-size: 10
        max-active: 100
        min-idle: 10
        max-wait: 60000
        pool-prepared-statements: true
        max-pool-prepared-statement-per-connection-size: 20
        time-between-eviction-runs-millis: 60000
        min-evictable-idle-time-millis: 300000
        validation-query: SELECT 1
        test-while-idle: true
        test-on-borrow: true
        test-on-return: false
        stat-view-servlet:
          enabled: true
          url-pattern: /druid/*
          login-username: admin
          login-password: 123123
        filter:
          stat:
            log-slow-sql: true
            slow-sql-millis: 1000
            merge-sql: false
          wall:
            config:
              multi-statement-allow: true
```

### shiro JWT 配置
1. ShiroConfig配置
```java
/**
 * Shiro配置
 */
@Configuration
public class ShiroConfig {

    @Bean("sessionManager")
    public SessionManager sessionManager(){
        DefaultWebSessionManager sessionManager = new DefaultWebSessionManager();
        sessionManager.setSessionValidationSchedulerEnabled(true);
        sessionManager.setSessionIdCookieEnabled(true);
        return sessionManager;
    }

    @Bean("securityManager")
    public SecurityManager securityManager(OAuth2Realm oAuth2Realm, SessionManager sessionManager) {
        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
        securityManager.setRealm(oAuth2Realm);
        securityManager.setSessionManager(sessionManager);

        return securityManager;
    }

    @Bean("shiroFilter")
    public ShiroFilterFactoryBean shirFilter(SecurityManager securityManager) {
        ShiroFilterFactoryBean shiroFilter = new ShiroFilterFactoryBean();
        shiroFilter.setSecurityManager(securityManager);

        //oauth过滤
        Map<String, Filter> filters = new HashMap<>();
        filters.put("oauth2", new OAuth2Filter());
        shiroFilter.setFilters(filters);

        Map<String, String> filterMap = new LinkedHashMap<>();
        filterMap.put("/druid/**", "anon");
        filterMap.put("/app/**", "anon");
        filterMap.put("/login", "anon");
        filterMap.put("/**", "oauth2");
        shiroFilter.setFilterChainDefinitionMap(filterMap);

        return shiroFilter;
    }

    @Bean("lifecycleBeanPostProcessor")
    public LifecycleBeanPostProcessor lifecycleBeanPostProcessor() {
        return new LifecycleBeanPostProcessor();
    }

    @Bean
    public DefaultAdvisorAutoProxyCreator defaultAdvisorAutoProxyCreator() {
        DefaultAdvisorAutoProxyCreator proxyCreator = new DefaultAdvisorAutoProxyCreator();
        proxyCreator.setProxyTargetClass(true);
        return proxyCreator;
    }

    @Bean
    public AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor(SecurityManager securityManager) {
        AuthorizationAttributeSourceAdvisor advisor = new AuthorizationAttributeSourceAdvisor();
        advisor.setSecurityManager(securityManager);
        return advisor;
    }

}

```

2. OAuth2Filer配置
```java
/**
 * oauth2过滤器
 */
public class OAuth2Filter extends AuthenticatingFilter {

    @Override
    protected AuthenticationToken createToken(ServletRequest request, ServletResponse response) throws Exception {
        //获取请求token
        String token = getRequestToken((HttpServletRequest) request);

        if(StringUtil.isBlank(token)){
            return null;
        }

        return new OAuth2Token(token);
    }

    @Override
    protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) {
        if(((HttpServletRequest) request).getMethod().equals(RequestMethod.OPTIONS.name())){
            return true;
        }

        return false;
    }

    @Override
    protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception {
        //获取请求token，如果token不存在，直接返回401
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        String token = getRequestToken((HttpServletRequest) request);
        if(StringUtil.isBlank(token)){
            HttpServletResponse httpResponse = (HttpServletResponse) response;
            httpResponse.setHeader("Access-Control-Allow-Credentials", "true");
            httpResponse.setHeader("Access-Control-Allow-Origin", httpServletRequest.getHeader("Origin"));

            JSONObject json = new JSONObject();
            json.put("code", "401");
            json.put("msg", "invalid token");

            httpResponse.getWriter().print(json);

            return false;
        }

        return executeLogin(request, response);
    }

    @Override
    protected boolean onLoginFailure(AuthenticationToken token, AuthenticationException e, ServletRequest request, ServletResponse response) {
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        httpResponse.setContentType("application/json;charset=utf-8");
        httpResponse.setHeader("Access-Control-Allow-Credentials", "true");
        httpResponse.setHeader("Access-Control-Allow-Origin", httpServletRequest.getHeader("Origin"));
        try {
            //处理登录失败的异常
            Throwable throwable = e.getCause() == null ? e : e.getCause();

            JSONObject json = new JSONObject();
            json.put("code", "401");
            json.put("msg", throwable.getMessage());

            httpResponse.getWriter().print(json);
        } catch (IOException e1) {

        }

        return false;
    }

    /**
     * 获取请求的token
     */
    private String getRequestToken(HttpServletRequest httpRequest){
        //从header中获取token
        String token = httpRequest.getHeader("token");

        //如果header中不存在token，则从参数中获取token
        if(StringUtil.isBlank(token)){
            token = httpRequest.getParameter("token");
        }

        return token;
    }
}

```

3. OAuth2Realm配置
```java
/**
 * 认证
 */
@Component
public class OAuth2Realm extends AuthorizingRealm {
    @Autowired
    private ManagerService managerService;

    @Override
    public boolean supports(AuthenticationToken token) {
        return token instanceof OAuth2Token;
    }

    /**
     * 授权(验证权限时调用, 控制role 和 permissins时使用)
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        ManagerInfo manager = (ManagerInfo)principals.getPrimaryPrincipal();
        Integer managerId = manager.getManagerId();

        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();

        // 模拟权限和角色
        Set<String> permsSet = new HashSet<>();
        Set<String> roles = new HashSet<>();
        if (managerId == 1) {
            // 超级管理员-权限
            permsSet.add("delete");
            permsSet.add("update");
            permsSet.add("view");

            roles.add("admin");
        } else {
            // 普通管理员-权限
            permsSet.add("view");

            roles.add("test");
        }

        info.setStringPermissions(permsSet);
        info.setRoles(roles);

        return info;
    }

    /**
     * 认证(登录时调用)
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        String accessToken = (String) token.getPrincipal();

        //根据accessToken，查询用户信息
        ManagerToken managerToken = managerService.queryByToken(accessToken);
        //token失效
        SimpleDateFormat sm = new SimpleDateFormat("yyyyMMddHHmmss");
        Date expireTime;
        boolean flag = true;
        try {
            expireTime     = sm.parse(managerToken.getExpireTime());
            flag = managerToken == null || expireTime.getTime() < System.currentTimeMillis();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if(flag){
            throw new IncorrectCredentialsException("token失效，请重新登录");
        }

        //查询用户信息
        ManagerInfo managerInfo = managerService.getManagerInfo(managerToken.managerId);
        //账号锁定
        // if(managerInfo.getStatus() == 0){
        //     throw new LockedAccountException("账号已被锁定,请联系管理员");
        // }

        SimpleAuthenticationInfo info = new SimpleAuthenticationInfo(managerInfo, accessToken, getName());

        return info;
    }
}
```

4. OAuth2Token设置
```java
/**
 * token
 */
public class OAuth2Token implements AuthenticationToken {
    private String token;

    public OAuth2Token(String token){
        this.token = token;
    }

    @Override
    public String getPrincipal() {
        return token;
    }

    @Override
    public Object getCredentials() {
        return token;
    }
}
```

5.生成Token
```java
/**
 * 生成token
 */
public class TokenGenerator {

    public static String generateValue() {
        return generateValue(UUID.randomUUID().toString());
    }

    private static final char[] hexCode = "0123456789abcdef".toCharArray();

    public static String toHexString(byte[] data) {
        if (data == null) {
            return null;
        }
        StringBuilder r = new StringBuilder(data.length * 2);
        for (byte b : data) {
            r.append(hexCode[(b >> 4) & 0xF]);
            r.append(hexCode[(b & 0xF)]);
        }
        return r.toString();
    }

    public static String generateValue(String param) {
        try {
            MessageDigest algorithm = MessageDigest.getInstance("MD5");
            algorithm.reset();
            algorithm.update(param.getBytes());
            byte[] messageDigest = algorithm.digest();
            return toHexString(messageDigest);
        } catch (Exception e) {
            throw new RuntimeException("生成Token失败", e);
        }
    }
}
```

### Controller
```java
@RestController
public class WebController {

    private static final Logger LOGGER = LogManager.getLogger(WebController.class);

    @Autowired
    private ManagerService managerService;

    @RequestMapping("/login")
    public JSONObject login(@RequestParam("username") String username,
                            @RequestParam("password") String password) {
        JSONObject json = new JSONObject();
        json.put("result", false);
        json.put("msg", "账号或密码不正确");

        // 用户信息
        ManagerInfo managerInfo = managerService.getManagerInfo(username);
        // 账号不存在、密码错误
        if (managerInfo == null || !managerInfo.getPassword().equals(password)) {
            return json;
        }

        ManagerToken managerToken = managerService.saveToken(managerInfo.managerId);
        json.put("token", managerToken.token);
        json.put("result", true);
        json.put("msg", "登陆成功");

        return json;
    }

    /**
     * 必须带token请求, 否则返回401
     */
    @GetMapping("/article")
    public BaseResponse article() {
        return new BaseResponse(true, "article: You are already logged in", null);
    }

    /**
     * 不必带token也能请求到内容, 因为在shiro中配置了过滤规则
     */
    @GetMapping("/app/article")
    public BaseResponse appArticle() {
        return new BaseResponse(true, "appArticle: You are already logged in", null);
    }

    /**
     * 需要是超级管理员的token才能查看,
     */
    @GetMapping("/require_role")
    @RequiresRoles("admin")
    public BaseResponse requireRole() {
        return new BaseResponse(true, "You are visiting require_role", null);
    }

    /**
     * 需要有update权限才能访问
     */
    @GetMapping("/require_permission")
    // @RequiresPermissions(logical = Logical.AND, value = {"view", "edit"})
    @RequiresPermissions(logical = Logical.AND, value = {"update"})
    public BaseResponse requirePermission() {
        return new BaseResponse(true, "You are visiting permission require update", null);
    }

}
```

### Service
```java
@Service
public class ManagerService extends AbstractService {
    //12小时后过期
    private final static int EXPIRE = 3600 * 12 * 1000;

    public ManagerInfo getManagerInfo(String managerName) {
        String sql = "select a.managerName, a.managerLevelId,a.managerId, a.password"
                + " from manager a where a.managerName=?";
        ManagerInfo manager = jdbcDao.queryForObject(sql, new Object[] { managerName }, ManagerInfo.class);

        return manager;
    }

    public ManagerToken saveToken(Integer managerId) {
        ManagerToken managerToken = new ManagerToken();
        managerToken.managerId = managerId;

        //生成一个token
        managerToken.token = TokenGenerator.generateValue();
        //过期时间
        Date expireTime = new Date(System.currentTimeMillis() + EXPIRE);

        // 更新时间/过期时间
        SimpleDateFormat sm = new SimpleDateFormat("yyyyMMddHHmmss");
        Date systemDate = new Date();
        managerToken.updateTime = sm.format(systemDate);
        managerToken.expireTime = sm.format(expireTime);

        String sql = "insert into managertoken (managerId, token, updateTime, expireTime) values (?,?,?,?)"
                + " ON DUPLICATE KEY UPDATE token=?, updateTime=?, expireTime=?";
        jdbcDao.update(sql, new Object[]{managerToken.managerId, managerToken.token, managerToken.updateTime,
                managerToken.expireTime, managerToken.token, managerToken.updateTime, managerToken.expireTime});

        return managerToken;
    }

    @Transactional(propagation= Propagation.REQUIRED, isolation= Isolation.DEFAULT, readOnly = true, rollbackFor = Exception.class)
    public ManagerInfo getManagerInfo(Integer managerId) {
        if (managerId == null) {
            return null;
        }

        String sql = "select a.managerId, a.managerName, a.managerLevelId from manager a " +
                "where a.managerId=?";
        ManagerInfo manager = jdbcDao.queryForObject(sql, new Object[]{managerId}, ManagerInfo.class);

        return manager;
    }

    public ManagerToken queryByToken(String token) {
        if (token == null || "".equals(token)) {
            return null;
        }

        String sql = "select managerid managerId, token, expireTime, updateTime from managertoken where token=?";
        ManagerToken managerToken = jdbcDao.queryForObject(sql, new Object[]{token}, ManagerToken.class);

        return managerToken;
    }

}
```

### 下面还有一些基础的类或工具
![注释]()

## 测试
### 登陆
1. 首先登陆获取到token(localhost:8087/login?username=cscscs&password=4297f44b13955235245b2497399d7a93)
这里测试的用户有两个一个admin(超级管理员), 一个是cscscs

### 测试需要认证的接口
2. 不到token访问 (localhost:8087/article), 返回401, 说明现在认证已经成功啦
   接下来带token访问, 收到
   ```json
   {
       "result": true,
       "msg": "You are already logged in",
       "data": null
   }
   ```

### 测试不需要认证的接口
3. 在shiro的url过滤规则中, 设置了/app/**不需要认证
   不带token访问localhost:8087/app/article, 收到
   ```json
   {
       "result": true,
       "msg": "appArticle: You are already logged in",
       "data": null
   }
   ```

### 测试角色认证的接口
1. 用cscscs登陆的token, 访问localhost:8087/require_role, 返回401


2. 用admin登陆的token, 访问localhost:8087/require_role, 返回正确的json

### 测试权限认证的接口
localhost:8087/require_permission(该接口设置的update权限才能访问)
1. cscscs被赋予了view的权限, 用他的token访问需要update权限的接口, 返回

```json
{
    "msg": "没有权限，请联系管理员授权"
}
```

2. 用admin的token访问, 返回
```json
{
    "msg": "没有权限，请联系管理员授权"
}
```

## 结语
1. 首先大功告成, hah

2. 有很多不足可以改进, 如缓存啊, 更准确的权限设置啊, 但他可以帮你构建一个完整可用的JWT
   之前看了网上的例子, 理论很好, 例子却没跑通, aaaaa,,,

3. 谢谢人人开源啦

4. 走过慢慢长夜, 追不及你的长发, 拜拜




